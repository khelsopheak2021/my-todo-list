import { useState } from 'react';
import './assets/css/App.css';

import checkedIcon from './assets/images/checked.svg';
import uncheckIcon from './assets/images/uncheck.svg';
import deleteIcon from './assets/images/delete.svg';
import editIcon from './assets/images/edit.svg';
import saveIcon from './assets/images/save.svg';

// Define filter button as an array of object contain value and label
const filterButtons = [
  {
    label: 'All',
    value: null,
  },
  {
    label: 'Todo',
    value: false,
  },
  {
    label: 'Done',
    value: true,
  },
];

function App() {
  // Define constants store value of first tab (All tab)
  const firstTabValue = filterButtons[0].value;
  // Define hook activeTab with default value from firstTabValue
  const [activeTab, setActiveTab] = useState(firstTabValue);
  // Define hoolk todos list with default value empty array []
  const [todos, setTodos] = useState([]);
  // Define hook taskValue to store value from user input task in top header with default value empty string ''
  const [taskValue, setTaskValue] = useState('');
  // Define hook editItem to store object when editing item of todo list with default value empty object {}
  const [editItem, setEditItem] = useState({});
  // Define constants nextTodoId store id of todo with auto increment of last record of todo list
  const nextTodoId = todos.length > 0 ? todos[todos.length - 1].id + 1 : 1; // when list > 0 get last id then plus 1 else default id is starting from 1

  // Define constants filteredTodos store value from filtering based on activeTab variable when user change tab
  const filteredTodos =
    activeTab === firstTabValue
      ? todos
      : todos.filter((todo) => {
          return todo.done === activeTab;
        });

  // Define constants disabled store value prevent from taskValue empty and not allow user to click add button.
  const disabled = !taskValue || !taskValue.trim();

  return (
    <>
      <h1>Todo List</h1>
      <main className="todo-main">
        <div className="input-group">
          <input
            name="todo"
            autoFocus
            className="input-control"
            value={taskValue}
            onChange={(event) => {
              const value = event.target.value; // get value of input
              setTaskValue(value); // update taskValue state
            }}
          />
          <button
            className={`btn-add ${disabled ? 'disabled' : ''}`}
            disabled={disabled}
            title="Add New"
            onClick={() => {
              // check if user doen't input value of task then do nothing
              if (disabled) {
                return false;
              }

              // create clone todo
              const newTodos = [...todos];
              // add new todo
              newTodos.push({
                id: nextTodoId,
                done: false,
                task: taskValue.trim(),
              });
              // update todo with new user input
              setTodos(newTodos);
              // clear input form
              setTaskValue('');
            }}
          >
            Add
          </button>
        </div>

        <div className="tab-menu">
          {filterButtons.map((filterButton, index) => {
            // check if tab active
            const isActive = activeTab === filterButton.value;

            return (
              <button
                key={`filter-btn-${index}`}
                className={`btn-tab ${isActive ? 'active' : ''}`}
                title={filterButton.label}
                onClick={() => {
                  // check if current tab active then do nothing
                  if (isActive) {
                    return false;
                  }

                  // update active tab with current clicked tab value
                  setActiveTab(filterButton.value);
                }}
              >
                {filterButton.label}
              </button>
            );
          })}
        </div>

        <div className="card">
          <h2 className="header-title">
            {filteredTodos.length > 1
              ? `${filteredTodos.length} Tasks`
              : `${filteredTodos.length} Task`}
          </h2>
          {filteredTodos.map((todo, index) => {
            // check is user click editing item match with an id in the list of todo
            const isEditing = todo.id === editItem.id;

            return (
              <div key={`todo-item-${index}`} className="todo-item">
                <div className="todo-item-content">
                  {!isEditing && (
                    <>
                      {todo.done ? (
                        <img
                          className="todo-icon"
                          src={checkedIcon}
                          alt="checked"
                          onClick={() => {
                            // clone todos
                            const newTodos = [...todos];
                            // find index of cloned todos with an id
                            const foundIndex = newTodos.findIndex((item) => {
                              return item.id === todo.id;
                            });

                            // check if found index of todo in the cloned todo (< 0 no record)
                            if (foundIndex >= 0) {
                              newTodos[foundIndex].done = false; // update done to false (to uncheck)
                              setTodos(newTodos); // update state of todos with new update
                            }
                          }}
                        />
                      ) : (
                        <img
                          className="todo-icon"
                          src={uncheckIcon}
                          alt="uncheck"
                          onClick={() => {
                            // clone todos
                            const newTodos = [...todos];
                            // find index of cloned todos with an id
                            const foundIndex = newTodos.findIndex((item) => {
                              return item.id === todo.id;
                            });

                            // check if found index of todo in the cloned todo (< 0 no record)
                            if (foundIndex >= 0) {
                              newTodos[foundIndex].done = true; // update done to true (to check)
                              setTodos(newTodos); // update state of todos with new update
                            }
                          }}
                        />
                      )}
                    </>
                  )}

                  <div className="todo-body">
                    {isEditing ? (
                      <input
                        name={`todo-${index}`}
                        autoFocus
                        className="editing input-control"
                        value={editItem.task || ''}
                        onChange={(event) => {
                          const value = event.target.value; // getting input value form input form
                          setEditItem({ ...todo, task: value }); // update state of edit item with current editing value
                        }}
                      />
                    ) : (
                      <p className="todo-item-title">{todo.task}</p>
                    )}
                  </div>
                </div>
                {!todo.done && (
                  <div className="todo-item-action">
                    <img
                      className="todo-icon"
                      src={isEditing ? saveIcon : editIcon}
                      alt="edit"
                      title={isEditing ? 'Save Changes' : 'Edit'}
                      onClick={() => {
                        // if user already clicked edit
                        if (isEditing) {
                          // check if edit value from input is empty do nothing
                          if (!editItem.task || !editItem.task.trim()) {
                            return false;
                          }

                          // clone todos
                          const newTodos = [...todos];
                          // find index of cloned todos with an id
                          const foundIndex = newTodos.findIndex((item) => {
                            return item.id === todo.id;
                          });

                          // check if found index of todo in the cloned todo (< 0 no record)
                          if (foundIndex >= 0) {
                            newTodos[foundIndex].task = editItem.task; // updat cloned todo task with new value from user input
                            setTodos(newTodos); // update state of todos with new update
                            setEditItem({}); // reset editing item to empty object
                          }
                          return false;
                        }

                        // else mean user not yet click edit then set editing item with todo value
                        setEditItem(todo);
                      }}
                    />
                    {!isEditing && (
                      <img
                        className="todo-icon"
                        src={deleteIcon}
                        alt="delete"
                        title="Delete"
                        onClick={() => {
                          // filter todos that not match with an current selected id
                          const newTodos = todos.filter((todoInner) => {
                            return todo.id !== todoInner.id;
                          });

                          // udpate state of todos with filtered list
                          setTodos(newTodos);
                        }}
                      />
                    )}
                  </div>
                )}
              </div>
            );
          })}
        </div>
      </main>
    </>
  );
}

export default App;
